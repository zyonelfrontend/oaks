'use client';
import Image from 'next/image';
import Vector5 from '../../../public/assets/images/Vector-5.png';
import Link from 'next/link';
import Logo from '../../../public/assets/images/logo.png';
import AltLogo from '../../../public/assets/icon/alt_logo.svg';
import { useState } from 'react';
import FacebookIcon from '../../components/FacebookIcon';
import InstagramIcon from '../../components/InstagramIcon';
import TwitterIcon from '../../components/TwitterIcon';
import LinkedIn from '../../components/linkedin';
import { usePathname } from 'next/navigation';
import { TextInput } from '@/components/CustomInput';
import Formsy from 'formsy-react';

const Footer = () => {
  const pathname = usePathname();
  const [formData, setFormData] = useState({
    email: '',
  });
  const [email, setEmail] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [newsletterError, setNewsletterError] = useState('');
  const handleEmailChange = (e) => setEmail(e.target.value);
  const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsSubmitting(true);

    const response = await fetch('/api/subscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    });

    if (response.ok) {
      setSuccessMessage('success');
      console.log('Email sent!');

      // Reset form data after successful registration
      setFormData({
        email: '',
      });
    } else {
      const responseData = await response.json();
      if (
        response.status === 400 &&
        responseData.error === 'Email already subscribed'
      ) {
        setSuccessMessage('exists');
        console.error('Email already subscribed');
      } else {
        setSuccessMessage('failure');
        console.error(
          'Failed to submit email:',
          responseData.error || 'Unknown error',
        );
      }
    }
    setIsSubmitting(false);

    setTimeout(() => {
      setSuccessMessage('');
    }, 5000);
  };

  //   try {
  //     if (!email || !emailRegex.test(email)) {
  //       setNewsletterError("Invalid email");
  //       return;
  //     }
  //     alert("Form will be sent");
  //   } catch (error) {
  //     setNewsletterError("Network Error");
  //   }
  // };

  const useAltNewsletterSection =
    pathname.includes('blog') ||
    pathname.includes('about-us') ||
    pathname.includes('services');

  return (
    <>
      <footer>
        <div>
          <div
            className={`${
              useAltNewsletterSection ? 'major bg-opacity-10' : 'alternative'
            } d-flex flex-column align-items-center`}
          >
            <h3
              className={`${
                useAltNewsletterSection ? 'major_text' : 'alternative_text'
              } text-center`}
            >
              Subscribe to Newsletter
            </h3>
            <p
              className={`${
                useAltNewsletterSection
                  ? 'second_major_text'
                  : 'second_alternative_text'
              }   py-5 text-center`}
            >
              Enter your email address to register to our newsletter
              subscription delivered on regular basis!
            </p>
            {useAltNewsletterSection ? (
              <form
                onSubmit={handleSubmit}
                className="d-flex flex-column flex-md-row align-items-center new_subscribe-form"
              >
                <input
                  className="subscribe me-md-2"
                  name="email"
                  type="email"
                  value={formData.email}
                  onChange={(e) =>
                    setFormData({ ...formData, email: e.target.value })
                  }
                  // autoComplete="email"
                  placeholder="Enter your email address"
                />
                <button
                  type="submit"
                  className={`px-4 py-3 mt-3' ${
                    successMessage.includes('success')
                      ? 'bg-success text-white'
                      : successMessage.includes('failure')
                        ? 'bg-danger text-white'
                        : 'orange'
                  }`}
                  disabled={isSubmitting}
                >
                  {isSubmitting
                    ? 'Submitting...'
                    : successMessage === 'success'
                      ? 'Congratulations! You have subscribed'
                      : successMessage === 'exists'
                        ? 'subscribed already. Thank you.'
                        : successMessage === 'failure'
                          ? 'Unable to at the moment. Please, try again or contact administrator'
                          : 'Subscribe Now'}
                </button>
              </form>
            ) : (
              <form
                onSubmit={handleSubmit}
                className="d-flex flex-column flex-md-row align-items-center subscribe-form"
              >
                <input
                  className="subscribe me-md-2"
                  name="email"
                  type="email"
                  value={formData.email}
                  onChange={(e) =>
                    setFormData({ ...formData, email: e.target.value })
                  }
                  // autoComplete="email"
                  placeholder="Enter your email address"
                />
                <button
                  type="submit"
                  className={`px-4 py-3 mt-3' ${
                    successMessage.includes('success')
                      ? 'bg-success text-white'
                      : successMessage.includes('failure')
                        ? 'bg-danger text-white'
                        : 'orange'
                  }`}
                  disabled={isSubmitting}
                >
                  {isSubmitting
                    ? 'Submitting...'
                    : successMessage === 'success'
                      ? 'Congratulations! You have subscribed'
                      : successMessage === 'exists'
                        ? 'subscribed already. Thank you.'
                        : successMessage === 'failure'
                          ? 'Unable to at the moment. Please, try again or contact administrator'
                          : 'Subscribe Now'}
                </button>
              </form>
            )}
            {/* <div className="w-full text-center">
              <form
                action="/send-data-here"
                method="post"
                onSubmit={handleSubmit}
              >
                <label htmlFor="first"></label>
                <input
                  className="w-full max-w-[60rem] ${ useAltNewsletterSection ? 'bg-white' : 'bg-white/[0.05]'} py-6 px-8 text-[#C7C7C7] text-xl lg:text-2xl"
                    onChange={handleEmailChange}
                  type="text"
                  id="first"
                  name="first"
                  placeholder="Enter your email address"
                  value={email}
                />
                <small className="text-red-600">{newsletterError}</small>
                <button
                  type="submit"
                  className="bg-primary text-white font-semibold text-2xl py-6 px-[3.4rem] mt-10 w-full lg:w-auto  max-w-[60rem]"
                >
                  Contact Us Now
                </button>
              </form>
              <div className="w-20 h-5">
                <Image src={Vector5} className="" alt="" />
              </div>
            </div> */}
          </div>

          <div className="bottom_footer">
            <div className="container p-md-0">
              <div className="row align-items-start">
                <div className="col-md-4 p-4 p-md-0">
                  <div className="">
                    <AltLogo />
                  </div>
                  <p className="">
                    Oasis and Oaks is an educational training and resource
                    provider, that helps you achieve your goals.
                  </p>
                </div>
                <div className="col-md-8 ps-4 ps-md-0">
                  <div className="d-flex flex-column flex-md-row align-items-start justify-content-center">
                    <div>
                      <h4 className="mb-2 mb-md-5 mt-3 mt-md-0">About Us</h4>
                      <ul className="">
                        <li>
                          <Link href="/home" className="">
                            Home
                          </Link>
                        </li>
                        <li>
                          <Link href="/about-us#whoweare" className="">
                            Who We Are
                          </Link>
                        </li>
                        <li>
                          <Link href="/about-us#teachingmethod" className="">
                            Teaching Method
                          </Link>
                        </li>
                        <li>
                          <Link href="/home#testimonials" className="">
                            Testimonials
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="mx-md-5 px-md-3">
                      <h4 className="mb-2 mb-md-5 mt-3 mt-md-0">
                        Our Services
                      </h4>
                      <ul className="flex flex-col  items-center md:items-start gap-3 text-[#1B1B1B]">
                        <li>
                          <Link href="/study-abroad" className="">
                            Study Abroad
                          </Link>
                        </li>
                        <li>
                          <Link href="/ielts" className="">
                            IELTS Examination
                          </Link>
                        </li>
                        <li>
                          <Link href="/ielts" className="">
                            Language Development
                          </Link>
                        </li>
                        <li>
                          <Link href="/study-abroad" className="">
                            Application Process
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div>
                      <h4 className="mb-2 mb-md-5 mt-3 mt-md-0">Quick links</h4>
                      <ul className="flex flex-col  items-center md:items-start gap-3 text-[#1B1B1B]">
                        <li>
                          <Link href="/blogs" className="">
                            Blogs
                          </Link>
                        </li>
                        <li>
                          <Link href="/service-form" className="">
                            Events
                          </Link>
                        </li>
                        <li>
                          <Link href="/service-form#appointment" className="">
                            Book an Appointment
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="ms-md-5 ps-md-3">
                      <h4 className="mb-2 mb-md-5 mt-3 mt-md-0">
                        We are Social
                      </h4>
                      <ul className="d-flex social_links align-items-center justify-content-center">
                        {/* <Link href="/" className="">
                          <li className="ms-0">
                            <LinkedIn />
                          </li>
                        </Link> */}
                        <Link
                          href="https://www.facebook.com/OasisnOaks?mibextid=ZbWKwL"
                          className=""
                        >
                          <li className="">
                            <FacebookIcon />
                          </li>
                        </Link>
                        <Link href="https://x.com/oasisandoaks/" className="">
                          <li className="">
                            <TwitterIcon />
                          </li>
                        </Link>
                        <Link
                          href="https://www.instagram.com/oasis_oaks?igsh=MWYzeHYwMHd3N3U3cg=="
                          className=""
                        >
                          <li className="">
                            <InstagramIcon />
                          </li>
                        </Link>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div className="border-footer my-3 my-md-5" />
              <div className="footer_bottom d-flex flex-column flex-md-row align-items-center justify-content-between">
                <p className="">©️2024 Oasis & Oaks. All rights reserved. </p>
                <ul className="d-flex mt-3 mt-md-0">
                  <li>
                    <Link href="/" className="">
                      Terms
                    </Link>
                  </li>
                  <li>
                    <Link href="/" className="mx-4">
                      privacy
                    </Link>
                  </li>
                  {/* <li>
                    <Link href="/" className="">
                      Cookies
                    </Link>
                  </li> */}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
