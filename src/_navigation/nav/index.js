'use client';
import Link from 'next/link';
import Image from 'next/image';
import React, { useRef, useEffect, useState } from 'react';
import oasis from '../../../public/assets/images/logo-alt.png';
import oasis2 from '../../../public/assets/images/logo-main.png';
import BurgerMenuIcon from '../../../public/assets/icon/burger-menu.svg';
import CloseIcon from '../../../public/assets/icon/close-x.svg';
import classNames from 'classnames';
import { menu } from '@/_constants/enum.constant';
import useOnClickOutside from '@/components/useOnClickOutside';
import { usePathname } from 'next/navigation';
import { useRouter } from 'next/router';
// eslint-disable-next-line react/display-name

const Navbar = ({ showBg }) => {
  const pathname = usePathname();
  const [mobileMenu, setMobileMenu] = useState(false);

  const useAltNewsletterSection =
    pathname.includes('about-us') ||
    pathname.includes('services') ||
    pathname.includes('service-form') ||
    pathname.includes('study-abroad') ||
    pathname.includes('ielts');

  const bgColor = () => {
    if (!showBg) return '';
    if (showBg && useAltNewsletterSection) return 'whiteBg';
    if (showBg && !useAltNewsletterSection) return 'darkBg';
  };

  return (
    <>
      <div
        className={`${bgColor()} __navbar d-flex container-fluid align-items-center`}
      >
        <div className="content w-100 mx-3 mx-md-5 px-md-3 h-100 d-flex align-items-center justify-content-between">
          {useAltNewsletterSection ? (
            <div className="logo d-flex align-items-center justify-content-start">
              <Link href="/" className="">
                <div className="img">
                  <Image src={oasis2} alt="..." className="w-100 h-100" />
                </div>
              </Link>
            </div>
          ) : (
            <div className="logo d-flex align-items-center justify-content-start">
              <Link href="/" className="">
                <div className="img">
                  <Image src={oasis} alt="..." className="w-100 h-100" />
                </div>
              </Link>
            </div>
          )}
          <div className="d-flex align-items-center">
            <ul className="mb-0 me-5 menu h-100 d-none d-md-flex align-items-center">
              {menu?.map((item) =>
                useAltNewsletterSection ? (
                  <Link
                    key={`menu-item-${item?.val + 1}`}
                    href={item?.link}
                    onClick={() => {}}
                    className={classNames('mx-4 px-2 text-nowrap h-100 nav1', {
                      active: item?.activeCode,
                    })}
                  >
                    <li className="text-capitalize h-100 d-flex align-items-center">
                      {item.label}
                    </li>
                  </Link>
                ) : (
                  <Link
                    key={`menu-item-${item?.val + 1}`}
                    href={item?.link}
                    onClick={() => {}}
                    className={classNames('mx-4 px-2 text-nowrap h-100 nav2', {
                      active: item?.activeCode,
                    })}
                  >
                    <li className="text-capitalize h-100 d-flex align-items-center">
                      {item.label}
                    </li>
                  </Link>
                ),
              )}
            </ul>
          </div>
          <div className="action d-flex align-items-center justify-content-end">
            <div className="contact d-none d-md-flex ">
              <Link href="/service-form">
                <button className="px-4 py-3">Contact us</button>
              </Link>
            </div>
            {useAltNewsletterSection ? (
              <BurgerMenuIcon
                onClick={() => setMobileMenu(true)}
                className={classNames('burgerIcon d-md-none d-inline-flex', {})}
              />
            ) : (
              <BurgerMenuIcon
                onClick={() => setMobileMenu(true)}
                className={classNames(
                  'burgerIcon2 d-md-none d-inline-flex',
                  {},
                )}
              />
            )}
          </div>
        </div>
        {mobileMenu && (
          <div
            className={classNames(
              'mobileMenu position-absolute',
              `${mobileMenu && 'showMenu'}`,
            )}
          >
            <div className="pt-2 pe-2 d-flex justify-content-end">
              <CloseIcon
                className=""
                onClick={() => setMobileMenu(false)}
                style={{
                  transform: 'scale(1)',
                  color: '#258575',
                  cursor: 'pointer',
                }}
              />
            </div>
            <div className="content px-4 pb-4">
              {useAltNewsletterSection ? (
                <div className="logo mb-4 d-flex align-items-center justify-content-center">
                  <Link
                    href="/"
                    className=""
                    onClick={() => setMobileMenu(false)}
                  >
                    <div className="img">
                      <Image src={oasis2} alt="..." className="w-100 h-100" />
                    </div>
                  </Link>
                </div>
              ) : (
                <div className="logo d-flex align-items-center justify-content-center">
                  <Link
                    href="/"
                    className=""
                    onClick={() => setMobileMenu(false)}
                  >
                    <div className="img">
                      <Image src={oasis} alt="..." className="w-100 h-100" />
                    </div>
                  </Link>
                </div>
              )}
              <ul className="menu d-flex flex-column align-items-center">
                {menu?.map((item) => (
                  <Link
                    key={`menu-item-${item?.val + 1}`}
                    href={item?.link}
                    onClick={() => setMobileMenu(false)}
                    className={classNames('mx-3 text-nowrap h-100', {
                      active: item?.activeCode,
                    })}
                  >
                    <li className="text-capitalize h-100 d-flex align-items-center p-3">
                      {item.label}
                    </li>
                  </Link>
                ))}
              </ul>
              <div className="d-flex justify-content-center">
                <div className="" onClick={() => setMobileMenu(false)}>
                  <Link href="/service-form">
                    <button className="px-4 py-3">Contact us</button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Navbar;
