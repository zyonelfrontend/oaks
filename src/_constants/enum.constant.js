export const menu = [
  { val: 1, link: '/about-us', label: 'About us', activeCode: 1 },
  { val: 2, link: '/study-abroad', label: 'Study Abroad', activeCode: 2 },
  { val: 3, link: '/ielts', label: 'IELTS Exam', activeCode: 3 },
  { val: 4, link: '/blogs', label: 'Blog', activeCode: 4 },
];
