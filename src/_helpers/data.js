import CarType from '../../public/_assets/images/addedasset.png';

export const maintenanceData = [
  {
    id: 0,
    center: 'Drive 45 center',
    vehicleId: '#12345',
    vehicleName: 'Toyota Corolla',
    date: '06/05/2022',
    status: 'Checked',
  },
  {
    id: 1,
    center: 'Drive 45 center',
    vehicleId: '#12345',
    vehicleName: 'Toyota Corolla',
    date: '06/05/2022',
    status: 'Missed',
  },
  {
    id: 2,
    center: 'Drive 45 center',
    vehicleId: '#12345',
    vehicleName: 'Toyota Corolla',
    date: '06/05/2022',
    status: 'Pending',
  },
  {
    id: 3,
    center: 'Drive 45 center',
    vehicleId: '#12345',
    vehicleName: 'Toyota Corolla',
    date: '06/05/2022',
    status: 'Checked',
  },
];

export const paymentHistoryData = [
  {
    id: 0,
    paymentId: '#12345',
    email: 'Jel@gmail.com',
    paymentMethod: 'Paystack',
    subscription: '3 years',
    date: '06/05/2022',
    status: 'Checked',
  },
  {
    id: 1,
    paymentId: '#12345',
    email: 'Jel@gmail.com',
    paymentMethod: 'Paystack',
    subscription: '3 years',
    date: '06/05/2022',
    status: 'Checked',
  },
  {
    id: 2,
    paymentId: '#12345',
    email: 'Jel@gmail.com',
    paymentMethod: 'Paystack',
    subscription: '3 years',
    date: '06/05/2022',
    status: 'Checked',
  },
];

export const assetAddedData = [
  {
    id: 0,
    VehicleType: 'Audi',
    CarImage: CarType,
    Model: 'Corrola LE CVT',
    Unit: '5',
    payment: '₦100,000',
    duration: '36 months',
    status: 'Pending',
  },
  {
    id: 1,
    VehicleType: 'Audi',
    CarImage: CarType,
    Model: 'Corrola LE CVT',
    Unit: '10',
    payment: '₦100,000',
    duration: '36 months',
    status: 'Pending',
  },
];

export const fastTrackData = [
  {
    id: 0,
    paymentId: '#12345',
    email: 'Jel@gmail.com',
    type: 'Tenure reuction',
    paymentMethod: '3 years',
    subscription: '₦100,000',
    date: '06/05/2022',
    status: 'Active',
  },
  {
    id: 1,
    paymentId: '#12345',
    email: 'Jel@gmail.com',
    type: 'Tenure reuction',
    paymentMethod: '3 years',
    subscription: '₦100,000',
    date: '06/05/2022',
    status: 'Active',
  },
  {
    id: 2,
    paymentId: '#12345',
    email: 'Jel@gmail.com',
    type: 'Tenure reuction',
    paymentMethod: '3 years',
    subscription: '₦100,000',
    date: '06/05/2022',
    status: 'Active',
  },
];

export const reportData = [
  {
    id: 0,
    incidentId: 'Marcia Koch',
    vehicleId: '#12345',
    Category: 'Accident',
    Location: '061 Antonio Port',
    Description: 'Quos sunt dolorum..',
    State: 'Minor',
    date: '06/05/2022',
    status: 'Resolved',
  },
  {
    id: 1,
    incidentId: 'Marcia Koch',
    vehicleId: '#12345',
    Category: 'Accident',
    Location: '061 Antonio Port',
    Description: 'Quos sunt dolorum..',
    State: 'Minor',
    date: '06/05/2022',
    status: 'Pending',
  },
];

export const faqs = [
  {
    id: 0,
    question: 'What is Drive45?',
    ans: 'Drive45 is a vehicle subscription platform that enables ease of vehicle ownership and mobility for individuals and enterprises',
  },

  {
    id: 1,
    question:
      'What are the benefits of subscribing for a vehicle with Drive45?',
    ans: 'Convenience, All bundled services, Flexibility, Affordability, Choice',
  },

  {
    id: 2,
    question: 'Is it safe to subscribe for a vehicle with Drive45?',
    ans: 'Yes, it is. We prioritize the use of the latest security measures to protect your financial and other confidential information.',
  },
  {
    id: 3,
    question:
      'What are the fees associated with subscribing for a vehicle with Drive45?',
    ans: 'The subscription rentals can vary depending on the type of user (individual or enterprise) and the duration option a user selects to complete the rental subscription.',
  },
  {
    id: 4,
    question: 'What is the customer support process for Drive45?',
    ans: 'We offer customer support through two (2) channels including email and phone. You can find these contact information on our website.',
  },
  {
    id: 5,
    question:
      'What is the maximum number of vehicles you can subscribe to buy?',
    ans: '2 for an individual user and as many is required for an enterprise user',
  },
  {
    id: 6,
    question: 'What is the subscription duration? ',
    ans: 'The subscription duration ranges from 3-4 years for either pre-owned or brand new Sedan, SUV and Pickup trucks; while it ranges from 4-6 years for light duty, medium duty and heavy duty trucks.',
  },
  {
    id: 7,
    question: 'Who is entitled to subscribe on your platform?',
    ans: 'Salaried individuals, Entrepreneurs, Multinationals and other government/private businesses.',
  },
  {
    id: 8,
    question: 'What is the lowest amount one can subscribe to?',
    ans: 'The subscription fee is largely dependent on the subscription tenure. However, there are vehicles from N110,000 and above, pending how long one intends to subscribe.',
  },
];

export const filter = [
  {
    id: 0,
    props: 'Status',
  },

  {
    id: 1,
    props: 'Date',
  },
];
