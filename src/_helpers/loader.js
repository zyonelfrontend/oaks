import React from 'react';
// import PropTypes from 'prop-types';
// import './index.scss';

const Loader = (props) => {
  return (
    <div className="d-flex align-items-center justify-content-center w-100">
      <div className={props.className} style={props.style}>
        {props.children}
      </div>
    </div>
  );
};

export default Loader;

// Loader.propTypes = {
//    className: PropTypes.string,
//    style: PropTypes.object,
// };
