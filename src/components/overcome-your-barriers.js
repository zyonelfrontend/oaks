'use client';
import SuccessCard from './successCard';

const OvercomingYourBarrier = ({ successStories = [] }) => {
  return (
    <>
      <section className="overcoming">
        <div className="radial1"></div>
        <div className="radial2"></div>
        <div className="container">
          <div className="titles text-center">
            <p className="main_title">OVERCOME YOUR BARRIERS</p>
            <h3 className="sub_title mt-md-4 pt-3 text-center">
              Start Your Journey of Excellence
            </h3>
            <h3 className="sub_title mt-md-4 pt-md-3 text-center">
              and Success Today
            </h3>
          </div>
          <div className="process">
            <div className="row">
              {successStories.map(
                (
                  { title, description, description2, image, borderColor },
                  index,
                ) => (
                  <SuccessCard
                    key={`sucess-${index}`}
                    title={title}
                    description={description}
                    description2={description2}
                    image={image}
                    borderColor={borderColor}
                  />
                ),
              )}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default OvercomingYourBarrier;
