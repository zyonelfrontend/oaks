'use client';
import Image from 'next/image';
import Link from 'next/link';
import Arrow from '../../public/assets/images/arrow_chain.png';

const ServiceFormBanner = () => {
  return (
    <>
      <section className="serviceform d-flex flex-column justify-content-center align-items-center position-relative">
        <div className="banner_text">
          <h4>Services Form page</h4>
          <p className="mt-2 mt-md-0">
            If you are looking to study, develop a career, or settle in an
            English-speaking country, IELTS can pave the way for you in all
            these endeavors
          </p>
        </div>
        <div className="arrow">
          <Image src={Arrow} alt="..." />
        </div>
        <div className="radial1"></div>
      </section>
    </>
  );
};

export default ServiceFormBanner;
