'use client';
import Image from 'next/image';
import Link from 'next/link';
import Arrow from '../../public/assets/icon/Up Left.svg';
import Latest1 from '../../public/assets/images/latest1.png';
import Latest2 from '../../public/assets/images/latest2.png';
import Latest3 from '../../public/assets/images/latest3.png';
import Latest4 from '../../public/assets/images/latest4.png';
import { useRouter } from 'next/router';

const MorePost = ({ otherPosts }) => {
  return (
    <section className="container latest px-md-0">
      <div className="row py-5">
        <h4>Latest Posts</h4>
        <p className="pb-0 pt-3 pt-md-5">Here are our latest posts so far.</p>
        {otherPosts.map((otherPost) => (
          <div className="col-md-6" key={`post-${otherPost}`}>
            <div className="latest my-5">
              <div className="">
                <div className="latest_img">
                  <div>
                    <Image
                      src={otherPost?.blogcover_image}
                      alt="..."
                      width={800}
                      height={350}
                    />
                  </div>
                  <div className="d-flex my-3 align-items-center">
                    <div className="categories p-2 p-md-3">
                      {otherPost?.subtitle}
                    </div>
                    <div className="ms-2 ms-md-3 date">{otherPost?.date}</div>
                  </div>
                  <div className="title">{otherPost?.title}</div>
                  <div className="my-3 content">{otherPost?.description}</div>
                </div>
                <Link
                  className="link"
                  href={'/blog-detail/' + otherPost?.slug}
                  passHref
                >
                  Read Post <Arrow className="arrow" />
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
};

export default MorePost;
