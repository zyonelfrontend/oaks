'use client';
import Image from 'next/image';

const Info = ({ info = [], bgColor }) => {
  return (
    <>
      <section style={{ backgroundColor: bgColor }}>
        <div className="container me-lg-0 pe-lg-0 last_section">
          {/* <div className="row"> */}
          <div className="">
            {info.map(({ bgImg, bgImg2, subtitle2, subtitle }, index) => (
              <div key={`info-${index}`} className="row align-items-center">
                <div className="col-lg-6 mt-5">
                  <p className="">{subtitle}</p>
                  <p className="mt-4  ">{subtitle2}</p>
                </div>
                <div className="col-lg-6 relative mt-5 mt-md-0">
                  <div className="position-relative">
                    <Image src={bgImg2} className="" alt="" />
                    <div className="second_img position-absolute">
                      <Image src={bgImg} className="" alt="" />
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
          {/* </div> */}
        </div>
      </section>
    </>
  );
};

export default Info;
