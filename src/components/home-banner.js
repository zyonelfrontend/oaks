'use client';
import Image from 'next/image';
import HomeBannerImage from '../../public/assets/images/home-banner.png';
import Link from 'next/link';
import { usePathname } from 'next/navigation';

const HomeBanner = () => {
  const pathname = usePathname();
  return (
    <>
      <section className="home-banner">
        <div className="container">
          <div className="row justify-content-center align-items-center">
            <div className="col-md-6">
              <div className="ps-3 ps-md-0">
                <h1 className="text_banner">Scale your Hurdles</h1>
                <h1 className="text_banner">
                  Lead The Way to your{' '}
                  <span className="text-success">Future</span>
                </h1>
              </div>
              <div className="sub_text1 my-4 ps-3 ps-md-0">
                <p className="">
                  Connect with your goals - study, academic, career, language,
                  professional, and personal goals.
                </p>
              </div>
              <div className="sub_text2 ps-3 ps-md-0">
                <p className="">
                  We can help you gain mastery of what matters to you, Learn
                  from the Experts!
                </p>
              </div>

              <div className="d-flex flex-column flex-lg-row w-100 ps-lg-0 align-items-lg-center mb-4">
                <button className="connect px-4 py-3">
                  <Link href="/service-form">Connect with Us</Link>
                </button>
                <button className="learn px-4 py-3 mt-3 mt-lg-0 ms-lg-4">
                  <Link href="/about-us">Learn More</Link>
                </button>
              </div>
            </div>
            <div className="col-md-6">
              <Image
                className="home_banner_img"
                src={HomeBannerImage}
                alt="home banner"
                priority
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default HomeBanner;
