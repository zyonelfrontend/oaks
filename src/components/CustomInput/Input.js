import React, { useState } from 'react';
import { withFormsy } from 'formsy-react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

/// ICON IMPORTS ///
import ErrorIcon from '../../../public/assets/icon/Error.svg';

export const TextInput = withFormsy((props) => {
  const [focused, setFocused] = useState(false);
  const changeValue = (e) => {
    // setValue() will set the value of the component, which in
    // turn will validate it and the rest of the form
    // Important: Don't skip this step. This pattern is required
    // for Formsy to work.
    props.setValue(e.currentTarget.value);
    typeof props.onValueChange === 'function' &&
      props.onValueChange(e.currentTarget.value);
    if (props.handleAmount) {
      props.handleAmount(e.currentTarget.value);
    }
    if (props.handleAddress) {
      props.handleAddress(e.currentTarget.value);
    }
    if (props.handlePostal) {
      props.handlePostal(e.currentTarget.value);
    }
    if (props.valError !== '') {
      typeof props.clearError === 'function' && props.clearError();
    }
  };

  // props.errorMessage comes only if the component is invalid
  const errorMessage = props.errorMessage || props.valError;

  return (
    <div
      className={classNames('text-input', props.className, {
        error: !!errorMessage && !props.isPristine,
      })}
    >
      <div
        className={classNames(' position-relative', {
          focus: focused,
          filled: !!props.value,
        })}
      >
        <label htmlFor={props.name}>{props.label}</label>
        {props.prefix}
        {props.leftIcon}
        <input
          id={props.id}
          name={props.name}
          type={props.type}
          className="w-100"
          onFocus={() => setFocused(true)}
          onBlur={() => setFocused(false)}
          value={props.value || ''}
          onChange={changeValue}
          required={props.required}
          disabled={props.disabled}
          autoFocus={props.autoFocus}
          autoComplete={props.autoComplete}
          placeholder={props.placeholder}
        />
        {props.rightIcon}
      </div>
      {!!errorMessage && !props.isPristine && (
        <div className="error ps-1 pt-1 d-flex align-items-center">
          <ErrorIcon />
          <p className="ps-1" style={{ color: 'red' }}>
            {errorMessage}
          </p>
        </div>
      )}
    </div>
  );
});

TextInput.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string.isRequired,
  className: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  autoFocus: PropTypes.bool,
  autoComplete: PropTypes.string,
};
