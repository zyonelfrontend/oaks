'use client';
import Image from 'next/image';

const Steps = ({
  bgColor = '#258575',
  guideSteps = [],
  subGuideSteps = [],
}) => {
  return (
    <>
      <section>
        <div className="steps">
          <div className="green_path">
            <div className="">
              {guideSteps.map(
                (
                  {
                    mainTitle,
                    title,
                    subtitle,
                    subtitle2,
                    subtitle3,
                    subtitle4,
                    stepImg,
                  },
                  index,
                ) => (
                  <div
                    key={`steps-${index}`}
                    className={`d-flex flex-column flex-md-row justify-content-between py-4 align-items-center ${index % 2 === 0 ? 'flex-md-row pe-md-0' : 'flex-md-row-reverse ps-md-0'}`}
                  >
                    <div className="px-3 px-md-5 py-4 m-md-5 step_text">
                      <p className="main">{mainTitle}</p>
                      <h4 className="my-2 my-md-4">{title}</h4>
                      <p className="content">{subtitle}</p>
                      <p className="content my-3">{subtitle2}</p>
                      <p className="content">{subtitle3}</p>
                      <p className="content mt-3">{subtitle4}</p>
                    </div>
                    <div className="step_image py-md-5">
                      <Image src={stepImg} alt="" />
                    </div>
                  </div>
                ),
              )}
            </div>
          </div>

          <div className="">
            <div className="row">
              {subGuideSteps.map(
                (
                  { mainTitle, title, subtitle, subtitle2, subtitle3 },
                  index,
                ) => (
                  <div key={`steps-${index}`} className="col-md-6 my-3">
                    <div className="gradient mt-4 h-100">
                      <div className="px-4 py-5 step_text">
                        <p className="main">{mainTitle}</p>
                        <h4 className="my-2 my-md-4">{title}</h4>
                        <p className="content">{subtitle}</p>
                        <p className="content my-3">{subtitle2}</p>
                        <p className="content">{subtitle3}</p>
                      </div>
                    </div>
                  </div>
                ),
              )}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Steps;
