'use client';
import Image from 'next/image';
import Link from 'next/link';

const SameBanner = ({
  title,
  subtitle,
  BannerImg,
  BannerImg2,
  PointerImg,
  reverse = false,
}) => {
  return (
    <>
      <section className="about_banner">
        <div className="radial1"></div>
        <div className="radial2"></div>
        <div className="container">
          <div className="row align-items-center">
            <div className="col-md-6">
              <div className="">
                <h2 className="title">{title}</h2>
                <p className="paragraph">{subtitle}</p>
                <div className="d-flex w-100 align-items-center my-4">
                  <Link href="/service-form" className="">
                    <button className="connect w-100 px-4 py-3">
                      Connect with Us
                    </button>
                  </Link>
                  <Link href="/study-abroad" className="ms-3">
                    <button className="learn w-100 px-4 py-3">
                      Learn More
                    </button>
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="d-flex images">
                <div className="top-12 me-2">
                  <Image
                    className="mb-5"
                    //   width={315}
                    //   height={511}
                    src={BannerImg2}
                    alt=""
                  />
                </div>
                <div className="bottom-12 ms-2">
                  <Image
                    className="mt-5"
                    //   width={315}
                    //   height={511}
                    src={BannerImg}
                    alt="j"
                  />
                </div>
                {PointerImg && (
                  <Image
                    className="absolute right-20"
                    width={186}
                    height={63}
                    src={PointerImg}
                    alt="y"
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default SameBanner;
