'use client';
import Image from 'next/image';
import Student1a from '../../public/assets/images/student1a.png';
import Student1b from '../../public/assets/images/student1b.png';
import Student1c from '../../public/assets/images/student1c.png';
import Student1d from '../../public/assets/images/student1d.png';
import Student2a from '../../public/assets/images/woman on blue.png';
import Student2b from '../../public/assets/images/student2b.png';
import Student3a from '../../public/assets/images/student3a.png';
import Student3b from '../../public/assets/images/man on suit.png';
import Link from 'next/link';

const Student = ({ bgColor = '#f9fefd' }) => {
  return (
    <>
      <section style={{ backgroundColor: bgColor }}>
        <div className="custom-container student pe-0 me-0">
          <div className="row align-items-center">
            <div className="col-md-6 ms-md-0 py-5 px-5 pt-md-0">
              <h1>Happy Faces of Happy Students</h1>
              <p className="my-4">
                Would you like to be a part of our success stories? You too can
                have a journey of success to share about your academic, career,
                professional or language goals. With us, your goals are
                achievable!
              </p>
              <Link href="/home#testimonials" className="">
                <button className="connect px-4 py-3">
                  View our Testimonials
                </button>
              </Link>
            </div>
            <div className="col-md-6">
              <div className="d-flex align-items-center">
                <div className="">
                  <div>
                    <Image src={Student1a} alt="..." />
                  </div>
                  <div className="d-flex align-items-center my-2">
                    <div>
                      <Image src={Student1b} alt="..." />
                    </div>
                    <div className="mx-2">
                      <Image src={Student1c} alt="..." />
                    </div>
                  </div>
                  <div>
                    <Image src={Student1d} alt="..." />
                  </div>
                </div>
                <div className="mx-2">
                  <div>
                    <Image src={Student2a} alt="..." />
                  </div>
                  <div className="mt-2">
                    <Image src={Student2b} alt="..." />
                  </div>
                </div>
                <div className="">
                  <div>
                    <Image src={Student3a} alt="..." />
                  </div>
                  <div className="mt-2">
                    <Image src={Student3b} alt="..." />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Student;
