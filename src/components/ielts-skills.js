'use client';
import Image from 'next/image';
import IeltSkills from '../../public/assets/images/ieltsskills.png';

const SkillsIelts = () => {
  return (
    <>
      <section className="skill">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-md-6 order-2 order-md-1 my-3 my-md-0">
              <Image src={IeltSkills} alt="" />
            </div>
            <div className="col-md-6 order-1 order-md-2">
              <p className="ps-md-5">
                IELTS exam can be Academic or General, and it assesses four key
                skills - <b>Listening, Reading, Writing, and Speaking</b>.  Both
                versions provide valid and accurate assessment of the four
                language skills. While the Listening and Speaking section of
                both Academic and General exams are the same, the Reading and
                Writing Task 1 sections, differ. The Academic exam is for people
                applying for professional registration or higher education,
                particularly to universities in the UK, Ireland, Australia, New
                Zealand, the US and Canada while the General Training is for
                those migrating to Australia, Canada and the UK, or applying for
                training programs and work experience in an English-speaking
                environment. 
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default SkillsIelts;
