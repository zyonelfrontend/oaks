'use client';
import Image from 'next/image';
import WhyOasis from '../../public/assets/images/why-oasis.png';
import Link from 'next/link';
import Icon1 from '../../public/assets/icon/icon1.svg';
import Icon2 from '../../public/assets/icon/icon2.svg';
import Icon3 from '../../public/assets/icon/icon3.svg';
import Icon4 from '../../public/assets/icon/icon4.svg';

const WhyOasisSession = () => {
  return (
    <>
      <section className="why-oasis pb-5 pb-lg-0">
        <div className="row align-items-center">
          <div className="col-lg-6 ">
            <Image
              className="flex-1 ms-2 ms-lg-0 mb-0"
              // width={880}
              // height={739}
              src={WhyOasis}
              alt="whyoasis"
            />
          </div>
          <div className="col-lg-6 mt-5 mt-lg-0">
            <h5 className="title ps-4 ps-lg-0 mt-5">
              WE HELP YOU ACHIEVE YOUR GOALS
            </h5>
            <h2 className="sub_title ps-4 ps-lg-0 my-4 py-2 py-md-0 my-md-5 uppercase text-white">
              Why Oasis and Oaks
            </h2>
            <p className="text ps-4 ps-lg-0">
              Oasis & Oaks is an educational resource provider that helps you
              achieve your goals.
              <br /> We offer services in language development, IELTS
              Preparatory Class, Adult Education, Workplace/Organisational
              Development Training as well as securing genuine admissions and
              work placements in foreign countries.
            </p>
            <div className="d-flex flex-column flex-md-row  ps-4 ps-lg-0 me-3 my-5">
              <div className="properties p-3 m-2">
                <Icon1 />
                <p className="text-white mt-3">Located in 15 Countries</p>
              </div>
              <div className="properties p-3 m-2">
                <Icon2 />
                <p className="text-white mt-3">Our End to End Services</p>
              </div>
              <div className="properties p-3 m-2">
                <Icon3 />
                <p className="text-white mt-3">750+ Partner Institutions</p>
              </div>
              <div className="properties p-3 m-2">
                <Icon4 />
                <p className="text-white mt-3">High Visa Success Rate</p>
              </div>
            </div>
            <Link href="/about-us#why">
              <button className="properties_button ms-4 ms-lg-0 px-4 py-3 mb-5">
                Learn More
              </button>
            </Link>
          </div>
        </div>
      </section>
    </>
  );
};

export default WhyOasisSession;
