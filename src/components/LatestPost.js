'use client';
import Image from 'next/image';
import Link from 'next/link';
import Arrow from '../../public/assets/icon/Up Left.svg';
import Latest1 from '../../public/assets/images/latest1.png';
import Latest2 from '../../public/assets/images/latest2.png';
import Latest3 from '../../public/assets/images/latest3.png';
import Latest4 from '../../public/assets/images/latest4.png';
import { useRouter } from 'next/router';

const LatestPost = ({ post }) => {
  return (
    <div className="latest my-3">
      <div className="">
        <div className="">
          {/* {latest?.map(({ photo, category, date, title, content }, index) => ( */}
          <div className="">
            <div className="">
              <div>
                <Image
                  src={post?.frontmatter.blogcover_image}
                  alt="..."
                  width={900}
                  height={500}
                />
              </div>
              <div className="d-flex my-3 align-items-center">
                <div className="categories p-2 p-md-3">
                  {post?.frontmatter.subtitle}
                </div>
                <div className="ms-2 ms-md-3 date">
                  {post?.frontmatter.date}
                </div>
              </div>
              <div className="title">{post?.frontmatter.title}</div>
              <div className="my-3 content">
                {post?.frontmatter.description}
              </div>
            </div>
            <Link className="link" href={'/blog-detail/' + post?.slug} passHref>
              Read Post <Arrow className="arrow" />
            </Link>
          </div>
          {/* ))} */}
        </div>
      </div>
      {/* <Link
        className="text-success flex gap-3 items-center text-[1.8rem] font-bold"
        href={`/blogs`}
      >
        <span className="underline"> Read More </span>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="25"
          viewBox="0 0 24 25"
          fill="none"
        >
          <path
            d="M12.1462 7.04706L16.803 7.04705C17.3553 7.04705 17.803 7.49477 17.803 8.04705L17.803 12.7039"
            stroke="#258575"
            strokeWidth="1.5"
            strokeLinecap="round"
          />
          <path
            d="M17.0948 7.75419L7.19531 17.6537"
            stroke="#258575"
            strokeWidth="1.5"
            strokeLinecap="round"
          />
        </svg>
      </Link> */}
    </div>
  );
};

export default LatestPost;
