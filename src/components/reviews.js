'use client';
import Image from 'next/image';
import Link from 'next/link';
import GroupImage from '../../public/assets/images/groupie.png';
import ArrowRight from '../../public/assets/icon/yellow_arrow.svg';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import UserIcon from '../../public/assets/icon/Iconly/squared/avatar.svg';

const Review = () => {
  var settings = {
    dots: true,
    arrows: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipeToSlide: true,
  };
  const Reviews = [
    {
      icon: <UserIcon />,
      author: 'Oyewande Funmi',
      content:
        'I was referred by a colleague to commence my IELTS academics class with Oasis & Oaks. My band scores were always low except for listening before I commenced classes, but I am so grateful to my tutor who was never tired of me; always there to keep me going with words of encouragement. I was given more tasks to work on, strategies on how to tackle different question and followed up on me to ensure my assignments were done within the stipulated time. I can categorically say that my confidence was well built to face the exams with the mind of me succeeding. I attempted 3 times and finally got the desired band score in the 3rd attempt. Are you contemplating on where to register for your IELTS class??',
      content2: 'Oasis and Oaks is your right choice',
    },
    {
      icon: <UserIcon />,
      author: 'Oyewande Funmi',
      content:
        'I was referred by a colleague to commence my IELTS academics class with Oasis & Oaks. My band scores were always low except for listening before I commenced classes, but I am so grateful to my tutor who was never tired of me; always there to keep me going with words of encouragement. I was given more tasks to work on, strategies on how to tackle different question and followed up on me to ensure my assignments were done within the stipulated time. I can categorically say that my confidence was well built to face the exams with the mind of me succeeding. I attempted 3 times and finally got the desired band score in the 3rd attempt. Are you contemplating on where to register for your IELTS class??',
      content2: 'Oasis and Oaks is your right choice',
    },
    {
      icon: <UserIcon />,
      author: 'Oyewande Funmi',
      content:
        'I was referred by a colleague to commence my IELTS academics class with Oasis & Oaks. My band scores were always low except for listening before I commenced classes, but I am so grateful to my tutor who was never tired of me; always there to keep me going with words of encouragement. I was given more tasks to work on, strategies on how to tackle different question and followed up on me to ensure my assignments were done within the stipulated time. I can categorically say that my confidence was well built to face the exams with the mind of me succeeding. I attempted 3 times and finally got the desired band score in the 3rd attempt. Are you contemplating on where to register for your IELTS class??',
      content2: 'Oasis and Oaks is your right choice',
    },
    {
      icon: <UserIcon />,
      author: 'Oyewande Funmi',
      content:
        'I was referred by a colleague to commence my IELTS academics class with Oasis & Oaks. My band scores were always low except for listening before I commenced classes, but I am so grateful to my tutor who was never tired of me; always there to keep me going with words of encouragement. I was given more tasks to work on, strategies on how to tackle different question and followed up on me to ensure my assignments were done within the stipulated time. I can categorically say that my confidence was well built to face the exams with the mind of me succeeding. I attempted 3 times and finally got the desired band score in the 3rd attempt. Are you contemplating on where to register for your IELTS class??',
      content2: 'Oasis and Oaks is your right choice',
    },
  ];
  return (
    <>
      <section className="review" id="testimonials">
        <div>
          <div className="row">
            <div className="col-md-5 p-0 m-0">
              <div className="sweet">
                <div>
                  <h6 className="text-center">
                    LET&apos;S HEAR WHAT THEY HAVE TO SAY
                  </h6>
                </div>
                <div>
                  <h2 className="text-center">Sweet Words From Our Students</h2>
                </div>
                <div className="review_Image d-flex justify-content-center">
                  <Image
                    src={GroupImage}
                    // width={534}
                    // height={484}
                    alt="Group image"
                  />
                </div>
              </div>
            </div>
            <div className="col-md-7 p-0 m-0">
              <div className="words h-100 p-5">
                <Slider {...settings}>
                  {Reviews?.map(
                    ({ icon, author, content, content2 }, index) => (
                      <div key={`reviews-${index}`} className="outside-content">
                        <div className="content p-3 p-md-5 m-2 m-md-4">
                          <div>
                            <p>{content}</p>
                            <p className="content2">{content2}</p>
                            <div className="mt-5 d-flex align-items-center user">
                              {icon}
                              <h5 className="ms-2">{author}</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    ),
                  )}
                </Slider>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Review;
