'use client';
import Image from 'next/image';
import SideImg from '../../public/assets/images/skewed_img.png';

const WhyChooseOasis = ({ title, chooseOasis = [] }) => {
  return (
    <>
      <section className="why_oasis" id="why">
        <div className="">
          <div className="d-flex flex-column flex-md-row w-100">
            <div className="skewed_bg">
              <div className="">
                <h1 className="text-center">{title}</h1>
                <div className="row mt-3">
                  {chooseOasis.map(({ icon, iconText }, index) => (
                    <div
                      key={`oasis-${index}`}
                      className="col-md-10 col-lg-5 why_items p-3 m-1 m-md-3"
                    >
                      <div className="d-flex align-items-center">
                        {icon}
                        <p className="ms-3">{iconText}</p>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div className="skewed_img d-md-flex d-none"></div>
          </div>
        </div>
      </section>
    </>
  );
};

export default WhyChooseOasis;
