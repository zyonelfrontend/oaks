// 'use client';
import Image from 'next/image';
import Preparation from '../../public/assets/icon/preparation.svg';
import Language from '../../public/assets/icon/language.svg';
import Abroad from '../../public/assets/icon/abroad.svg';
import { useCallback, useState, useEffect } from 'react';
import classNames from 'classnames';
import Formsy from 'formsy-react';
import { TextInput } from './CustomInput';
import Select from 'react-dropdown-select';
import Link from 'next/link';
import { countries } from 'countries-list';
import 'react-intl-tel-input/dist/main.css';
import IntlTelInput from 'react-intl-tel-input';

// import ReactSelect from 'react-select';

const Form = () => {
  const [active, setActive] = useState(1);
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
  });
  const [secondFormData, setSecondFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
  });

  const [successMessage, setSuccessMessage] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [secondSuccessMessage, setSecondSuccessMessage] = useState('');
  const [secondisSubmitting, setSecondIsSubmitting] = useState(false);

  const [canSubmit, setCanSubmit] = useState(false);
  const handleClick = useCallback((n) => {
    setActive((p) => {
      if (n !== p) {
        return n;
      }
      return p;
    });
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsSubmitting(true);

    const response = await fetch('/api/consultation', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    });

    // Check the status code from the server response
    if (response.ok) {
      setSuccessMessage('success');
      console.log('Registration successful!');

      // Reset form data after successful registration
      setFormData({
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
      });
    } else {
      const responseData = await response.json();
      if (
        response.status === 400 &&
        responseData.error === 'Email already booked'
      ) {
        setSuccessMessage('exists');
        console.error('Email already booked');
      } else {
        setSuccessMessage('failure');
        console.error(
          'Failed to submit email:',
          responseData.error || 'Unknown error',
        );
      }
    }
    setIsSubmitting(false);

    setTimeout(() => {
      setSuccessMessage('');
    }, 5000);
  };
  const secondHandleSubmit = async (e) => {
    e.preventDefault();
    setSecondIsSubmitting(true);

    const response = await fetch('/api/session', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(secondFormData),
    });

    // Check the status code from the server response
    if (response.ok) {
      setSecondSuccessMessage('success');
      console.log('Registration successful!');

      // Reset form data after successful registration
      setSecondFormData({
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
      });
    } else {
      console.error('Error sending details');
      setSecondSuccessMessage('Details not sent');
    }
    setSecondIsSubmitting(false);

    setTimeout(() => {
      setSecondSuccessMessage('');
    }, 5000);
  };
  return (
    <>
      <section className="form-field" id="appointment">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6">
              <Link href="#first">
                <div
                  onClick={() => handleClick(1)}
                  className={classNames(
                    'd-flex flex-column flex-lg-row align-items-center tab p-4 text-nowrap',
                    { active: active === 1 },
                  )}
                >
                  <div>
                    <Preparation />
                  </div>
                  <div className="mt-3 mt-lg-0 text-center">
                    <h4>Exam Preparation (IELTS/TOEFL)</h4>
                    <p>
                      (IELTS) seeks to test students English Language
                      proficiency.{' '}
                    </p>
                  </div>
                </div>
              </Link>
              <Link href="#second">
                <div
                  onClick={() => handleClick(2)}
                  className={classNames(
                    'd-flex flex-column flex-lg-row align-items-center tab p-4 text-nowrap my-4',
                    { active: active === 2 },
                  )}
                >
                  <div>
                    <Abroad />
                  </div>
                  <div className="mt-3 mt-lg-0 text-center">
                    <h4>Study Abroad</h4>
                    <p>
                      Secure admission to one of the best institutions around
                      the world
                    </p>
                  </div>
                </div>
              </Link>
              <Link href="#third">
                <div
                  onClick={() => handleClick(3)}
                  className={classNames(
                    'd-flex flex-column flex-lg-row align-items-center tab p-4 text-nowrap',
                    { active: active === 3 },
                  )}
                >
                  <div>
                    <Language />
                  </div>
                  <div className="mt-3 mt-lg-0 text-center">
                    <h4>Editorial Services</h4>
                    <p>
                      We deliver impeccable service in editing, data entry and
                      data analysis
                    </p>
                  </div>
                </div>
              </Link>
            </div>
            <div className="col-lg-6 mt-3 mt-md-3">
              {active === 1 && (
                <Formsy
                  // onValidSubmit={handleSubmit}
                  id="first"
                  onValid={() => setCanSubmit(true)}
                  onInvalid={() => setCanSubmit(false)}
                  className="myform p-5"
                >
                  <div className="form-title mt-2 mb-3">
                    <h3 className="">Exam Preparation (IELTS/TOEFL)</h3>
                    <p className="mt-3  ">Please fill in the details below</p>
                  </div>

                  <div className="amount mt-4 mb-5">
                    <p>Amount to be paid</p>
                    <h5 className="mt-3">₦ 50,000.00</h5>
                  </div>

                  <div className="col-12 d-flex flex-column flex-md-row mt-2">
                    <button
                      onClick={() =>
                        window.open(
                          'https://paystack.com/pay/95-51cy203',
                          '_blank',
                        )
                      }
                      className="px-4 py-3"
                    >
                      Make Payment
                    </button>
                  </div>
                </Formsy>
              )}
              {active === 2 && (
                <form
                  id="second"
                  onSubmit={handleSubmit}
                  // onValidSubmit={handleSubmit}
                  // onValid={() => setCanSubmit(true)}
                  // onInvalid={() => setCanSubmit(false)}
                  className="myform p-5"
                >
                  <div className="form-title mt-2 mb-3">
                    <h3 className="">Study Abroad</h3>
                    <p className="mt-3  ">Please fill in the details below</p>
                  </div>

                  <div className="form col-md-12">
                    <label className="my-2">First name</label>
                    <input
                      name="firstName"
                      type="text"
                      value={formData.firstName}
                      onChange={(e) =>
                        setFormData({ ...formData, firstName: e.target.value })
                      }
                      wrapperClassName="input-wrapper"
                      mainWrapperClassName="w-100"
                      required
                      placeholder="Your First Name"
                      className="fw-normal w-100 w-md-50 rounded mb-3 mb-md-0  p-3 text-black ft"
                    ></input>
                  </div>
                  <div className="form col-md-12">
                    <label className="my-2">Last name</label>
                    <input
                      type="text"
                      name="lastName"
                      value={formData.lastName}
                      onChange={(e) =>
                        setFormData({ ...formData, lastName: e.target.value })
                      }
                      wrapperClassName="input-wrapper"
                      mainWrapperClassName="w-100"
                      placeholder="Your Last Name"
                      className="fw-normal w-100 w-md-50 rounded mb-3 mb-md-0  p-3 text-black ft"
                    />
                  </div>
                  <div className="form col-md-12">
                    <label className="my-2">Email</label>
                    <input
                      name="email"
                      type="email"
                      value={formData.email}
                      onChange={(e) =>
                        setFormData({ ...formData, email: e.target.value })
                      }
                      wrapperClassName="input-wrapper"
                      mainWrapperClassName="w-100"
                      placeholder="you@company.com"
                      className="fw-normal w-100 w-md-50 rounded mb-3 mb-md-0  p-3 text-black ft"
                    />
                  </div>
                  <div className="form col-md-12">
                    <label className="my-2">Phone number</label>
                    <div className="w-md-50 contact-form border-0 rounded ft">
                      <IntlTelInput
                        inputClassName="intl-tel-input "
                        preferredCountries={['NG']}
                        autoHideDialCode={true}
                        separateDialCode={true}
                        formatOnInit={true}
                        placeholder="Phone number"
                        format={(selectedCountry) => {
                          return `+${selectedCountry.dialCode} (${selectedCountry.iso2})`; // Format to include country code
                        }}
                        onPhoneNumberChange={(
                          isValid,
                          phoneNumber,
                          countryData,
                        ) => {
                          setFormData({
                            ...formData,
                            phoneNumber: phoneNumber,
                          });
                        }}
                      />
                    </div>
                  </div>
                  {/* <div className="amount mt-4 mb-5">
                    <p>Amount to be paid</p>
                    <h5 className="mt-3">₦ 50,000.00</h5>
                  </div> */}

                  <div className="col-12 d-flex flex-column flex-md-row mt-2">
                    {/* <Link href="/"> */}
                    <button
                      type="submit"
                      className={`px-4 py-3 ${
                        successMessage.includes('success')
                          ? 'bg-success text-white'
                          : successMessage.includes('failure')
                            ? 'bg-danger text-white'
                            : 'orange'
                      }`}
                      disabled={isSubmitting}
                    >
                      {isSubmitting
                        ? 'Submitting...'
                        : successMessage === 'success'
                          ? 'Congratulations! You have successfully booked for a free consultation'
                          : successMessage === 'exists'
                            ? 'You are already booked. Thank you.'
                            : successMessage === 'failure'
                              ? 'Unable to Book. Please, try again or contact administrator'
                              : 'Book a Free Consultation'}
                    </button>
                    {/* </Link> */}
                  </div>
                </form>
              )}
              {active === 3 && (
                <form
                  onSubmit={secondHandleSubmit}
                  id="third"
                  // onValidSubmit={handleSubmit}
                  // onValid={() => setCanSubmit(true)}
                  // onInvalid={() => setCanSubmit(false)}
                  className="myform p-5"
                >
                  <div className="form-title mt-2 mb-3">
                    <h3 className="">Editorial Services</h3>
                    <p className="mt-3  ">Please fill in the details below</p>
                  </div>

                  <div className="form col-md-12">
                    <label className="my-2">First name</label>
                    <input
                      name="firstName"
                      type="text"
                      value={secondFormData.firstName}
                      onChange={(e) =>
                        setSecondFormData({
                          ...secondFormData,
                          firstName: e.target.value,
                        })
                      }
                      wrapperClassName="input-wrapper"
                      mainWrapperClassName="w-100"
                      required
                      placeholder="Your First Name"
                      className="fw-normal w-100 w-md-50 rounded mb-3 mb-md-0  p-3 text-black ft"
                    ></input>
                  </div>
                  <div className="form col-md-12">
                    <label className="my-2">Last name</label>
                    <input
                      type="text"
                      name="lastName"
                      value={secondFormData.lastName}
                      onChange={(e) =>
                        setSecondFormData({
                          ...secondFormData,
                          lastName: e.target.value,
                        })
                      }
                      wrapperClassName="input-wrapper"
                      mainWrapperClassName="w-100"
                      placeholder="Your Last Name"
                      className="fw-normal w-100 w-md-50 rounded mb-3 mb-md-0  p-3 text-black ft"
                    />
                  </div>
                  <div className="form col-md-12">
                    <label className="my-2">Email</label>
                    <input
                      name="email"
                      type="email"
                      value={secondFormData.email}
                      onChange={(e) =>
                        setSecondFormData({
                          ...secondFormData,
                          email: e.target.value,
                        })
                      }
                      wrapperClassName="input-wrapper"
                      mainWrapperClassName="w-100"
                      placeholder="you@company.com"
                      className="fw-normal w-100 w-md-50 rounded mb-3 mb-md-0  p-3 text-black ft"
                    />
                  </div>
                  <div className="form col-md-12">
                    <label className="my-2">Phone number</label>
                    <div className="w-md-50 contact-form border-0 rounded ft">
                      <IntlTelInput
                        inputClassName="intl-tel-input "
                        preferredCountries={['NG']}
                        autoHideDialCode={true}
                        separateDialCode={true}
                        formatOnInit={true}
                        placeholder="Phone number"
                        format={(selectedCountry) => {
                          return `+${selectedCountry.dialCode} (${selectedCountry.iso2})`; // Format to include country code
                        }}
                        onPhoneNumberChange={(
                          isValid,
                          phoneNumber,
                          countryData,
                        ) => {
                          setSecondFormData({
                            ...secondFormData,
                            phoneNumber: phoneNumber,
                          });
                        }}
                      />
                    </div>
                  </div>
                  {/* <div className="amount mt-4 mb-5">
                    <p>Amount to be paid</p>
                    <h5 className="mt-3">₦ 50,000.00</h5>
                  </div> */}

                  <div className="col-12 d-flex flex-column flex-md-row mt-2">
                    {/* <Link href="/"> */}
                    <button
                      type="submit"
                      className={`px-4 py-3 ${
                        secondSuccessMessage.includes('success')
                          ? 'bg-success text-white'
                          : secondSuccessMessage.includes('failure')
                            ? 'bg-danger text-white'
                            : 'orange'
                      }`}
                      disabled={secondisSubmitting}
                    >
                      {secondisSubmitting
                        ? 'Submitting...'
                        : secondSuccessMessage === 'success'
                          ? 'Congratulations! You have successfully booked for an editorial service session'
                          : secondSuccessMessage === 'exists'
                            ? 'You are already booked. Thank you.'
                            : secondSuccessMessage === 'failure'
                              ? 'Unable to Book. Please, try again or contact administrator'
                              : 'Book a Session'}
                    </button>
                    {/* </Link> */}
                  </div>
                </form>
              )}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Form;
