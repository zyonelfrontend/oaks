/* eslint-disable no-unused-vars */
import React, { useCallback, useState, useRef } from 'react';
import * as ReactBootstrap from 'react-bootstrap';
import Image from 'next/image';
import Logo from '../../public/_assets/images/Logo.png';
import onClickOutside from './useOnClickOutside.js';
import Link from 'next/link';
import styles from './index.module.scss';
import classNames from 'classnames';

export default function Navbar({ showBg }) {
  const featuresRef = useRef();
  const [isOpen, setIsOpen] = useState(false);
  const [open, setOpen] = useState(false);
  const toggle = useCallback(() => setOpen((prev) => !prev), []);
  onClickOutside(featuresRef, () => {
    setIsOpen(false);
  });

  return (
    <ReactBootstrap.Navbar
      expanded={isOpen}
      expand="xl"
      ref={featuresRef}
      className={styles.bg}
      bg={showBg ? 'dark' : 'none'}
      variant="dark"
      fixed="top"
    >
      <Link className="ms-0 ms-md-5 brand" href="/">
        <Image
          priority
          height={60}
          width={180}
          src={Logo}
          className="logo"
          alt=""
        />
      </Link>
      <ReactBootstrap.Navbar.Toggle
        className={styles.hamburger}
        onClick={() => setIsOpen((p) => !p)}
        aria-controls="responsive-navbar-nav"
      />

      <ReactBootstrap.Navbar.Collapse
        className="justify-content-between pe-xl-5"
        id="responsive-navbar-nav"
      >
        <ReactBootstrap.Nav
          className={classNames(` ps-4 ps-md-5 navbar-links, ${styles.navbar}`)}
        >
          {/* <Link onClick={() => setIsOpen(false)} className="nav-link" to="/">
                  Home
               </Link> */}
          <Link href="/broadcasters">
            <span className={styles.navLink} onClick={() => setIsOpen(false)}>
              Broadcasters
            </span>
          </Link>
          <Link href="/listeners">
            <span className={styles.navLink} onClick={() => setIsOpen(false)}>
              Listeners
            </span>
          </Link>
          <Link href="/pricing">
            <span className={styles.navLink} onClick={() => setIsOpen(false)}>
              Pricing
            </span>
          </Link>
          <Link href="/download">
            <span className={styles.navLink} onClick={() => setIsOpen(false)}>
              Download
            </span>
          </Link>
          <Link
            href={{ pathname: 'https://app.waystream.io/' }}
            target="_blank"
          >
            <span className={styles.navLink} onClick={() => setIsOpen(false)}>
              Listen
            </span>
          </Link>
          {/* <Link href="/blog">
                  <span className={styles.navLink} onClick={() => setIsOpen(false)}>Blog</span>
               </Link> */}
          <Link
            href={{ pathname: 'https://docs.waystream.io/' }}
            target="_blank"
          >
            <span className={styles.navLink} onClick={() => setIsOpen(false)}>
              Documentation
            </span>
          </Link>
          <ReactBootstrap.Nav.Link
            type="button"
            href="https://app.waystream.io/login"
            target="_parent"
            style={{
              backgroundColor: 'transparent',
              border: '1px solid #fff',
              textTransform: 'none',
              fontSize: '14px',
              padding: '12px 10px',
            }}
            className={`${styles.button2} col-7 col-sm-5 col-md-3 col-lg-2 col-xl-1 text-white mx-xl-3 my-3 my-xl-0 btn ms-xl-auto`}
            rel="noreferrer"
          >
            Sign In
          </ReactBootstrap.Nav.Link>
          <ReactBootstrap.Nav.Link
            type="button"
            href="/signup"
            target="_parent"
            style={{
              backgroundColor: '#F53B00',
              textTransform: 'none',
              fontSize: '14px',
              padding: '12px 10px',
            }}
            className={`${styles.button} col-7 col-sm-5 col-md-3 col-lg-2 col-xl-2 text-white mx-xl-3 my-3 my-xl-0 btn`}
            rel="noreferrer"
          >
            Start Free Trial
          </ReactBootstrap.Nav.Link>
        </ReactBootstrap.Nav>
      </ReactBootstrap.Navbar.Collapse>
    </ReactBootstrap.Navbar>
  );
}
