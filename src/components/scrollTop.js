// components/ScrollToTopOnRouterChange.js

import { useEffect, useRef } from 'react';
import { useRouter } from 'next/router';

function ScrollToTopOnRouterChange() {
  const router = useRouter();
  const topRef = useRef(null);

  useEffect(() => {
    const handleRouteChangeComplete = () => {
      if (topRef.current) {
        topRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
      }
    };

    // Subscribe to router events
    router.events.on('routeChangeComplete', handleRouteChangeComplete);

    // Remove event listener on cleanup
    return () => {
      router.events.off('routeChangeComplete', handleRouteChangeComplete);
    };
  }, [router.events]);

  return (
    <div
      ref={topRef}
      style={{ position: 'absolute', top: 0, left: 0, visibility: 'hidden' }}
    />
  ); // Place the reference element at the top and hide it
}

export default ScrollToTopOnRouterChange;
