'use client';
import Image from 'next/image';
import Link from 'next/link';
import ArrowRight from '../../public/assets/icon/yellow_arrow.svg';

const Where = ({ studyLocations = [] }) => {
  return (
    <>
      <section className="where">
        <h2 className="title text-center px-4 px-md-0">
          Where are you thinking of Studying?
        </h2>
        <div className="container mt-5 pt-5">
          <div className="row justify-content-center">
            {studyLocations.map(({ image, country, title }, index) => (
              <div
                key={`study-location-${index}`}
                className="col-12 col-md-4 backgroundprops relative m-2"
                style={{ backgroundImage: `url(${image.src})` }}
              >
                <div className="initial_blur p-4">
                  <h6 className="text-white">{country}</h6>
                </div>
                <div className="img-overlay p-4 d-flex flex-column justify-content-center">
                  <h6 className="text-white">{country}</h6>
                  <p className="text-white my-3 my-md-2 my-lg-4">{title}</p>
                  <Link className="link" href="/blogs">
                    Learn More <ArrowRight />
                  </Link>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    </>
  );
};

export default Where;
