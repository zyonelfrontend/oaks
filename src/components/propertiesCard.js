import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import ArrowRight from './arrowRight';

const PropertiesCard = ({ description, icon }) => {
  return (
    <div className="properties">
      <p>{description}</p>
    </div>
  );
};

export default PropertiesCard;
