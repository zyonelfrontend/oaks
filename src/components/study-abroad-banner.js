'use client';
import Image from 'next/image';
import Banner from '../../public/assets/images/study-abroad-banner-img.png';
import Banner1 from '../../public/assets/images/study-abroad-banner-img1.png';
import Banner2 from '../../public/assets/images/study-abroad-banner-img2.png';
import Vector5 from '../../public/assets/images/arrow_chain.png';

const StudyAbroadBanner = () => {
  return (
    <section className="">
      <div>
        <div className="ielts position-relative">
          <div className="container my-5">
            <h1 className="pt-5 text-center">University Application Process</h1>
            <div className="text">
              <p className="py-3 py-md-5 text-center px-2 px-md-5">
                If you are looking for how to start your journey in this track,
                we&apos;re your haven and best <br /> bet, as we offer excellent
                services with track records to guarantee your success.
              </p>
            </div>
            <div className="position-absolute arrow">
              <Image src={Vector5} alt="" />
            </div>
          </div>
          <div className="d-flex flex-column flex-md-row justify-content-center align-items-center align-items-md-end pt-4">
            <div className="mx-4 mx-md-0">
              <Image src={Banner} alt="" />
            </div>
            <div className="mx-4 mx-md-0 my-3 my-md-0">
              <Image src={Banner1} alt="" />
            </div>
            <div className="mx-4 mx-md-0">
              <Image src={Banner2} alt="" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default StudyAbroadBanner;
