'use client';

const FourSkillsIelts = ({ bgColor = '#1B1B1B' }) => {
  const section = [
    {
      Title: 'Listening Section',
      content:
        'This assesses how well students understand ideas, recognise opinions and follow the development of an argument. The section consists of forty questions, broken down into four sub-sections of ten questions each',
    },
    {
      Title: 'Reading Section',
      content: `This evaluates how well students can read and identify general ideas, main ideas and details, and whether students understand the author's inferences and opinions. The section consists of forty questions with three passages  and a random combination of the IELTS Reading question types.`,
    },
    {
      Title: 'Writing Section',
      content:
        'This is sub-divided into two- tasks 1 and 2. Task 1 is a report to be written by students after assessing a chart, graph, map, table, or process diagram while task 2 is an essay. Students are expected to answer both questions in 60minutes. The section evaluates how well and accurately students can organise their ideas and write a response, along with an ability to use a  wide-range of vocabulary and accurate grammar.',
    },
    {
      Title: 'Speaking Test',
      content: `This assesses students' communication prowess, how well they can articulate opinions and information on everyday topics, common experiences, as well as express and justify their opinions. The section is sub-divided into three parts.`,
      subcontent: `-general questions on students' name, career, hobbies and preferences`,
      subcontent2: `-the use of a cue card containing a question students are expected to share their experience on in two minutes.`,
      subcontent3: `-students are expected to express their ideas on selected questions.`,
    },
  ];
  return (
    <>
      <section>
        <div className="container section">
          <div className="row">
            {section.map(
              (
                { Title, content, subcontent, subcontent2, subcontent3 },
                index,
              ) => (
                <div key={`steps-${index}`} className="col-md-6 mt-4">
                  <div className="gradient h-100">
                    <div className="px-4 px-md-5 py-3 step_text">
                      <h4 className="my-1 my-md-3">{Title}</h4>
                      <p className="content">{content}</p>
                      <p className="content mt-1">{subcontent}</p>
                      <p className="content mt-1">{subcontent2}</p>
                      <p className="content mt-1">{subcontent3}</p>
                    </div>
                  </div>
                </div>
              ),
            )}
            <div className="grid grid-cols-1 md:grid-cols-2 gap-12"></div>
          </div>
        </div>
      </section>
    </>
  );
};

export default FourSkillsIelts;
