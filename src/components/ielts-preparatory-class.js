'use client';
import Image from 'next/image';
import Img1 from '../../public/assets/images/preparatory-exam1.png';
import Img2 from '../../public/assets/images/prepareratory-exams2.png';

const IeltsPreparatoryClass = ({ bgColor = 'rgba(210, 255, 247, 0.3)' }) => {
  return (
    <>
      <section style={{ backgroundColor: bgColor }} className="exam">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-md d-none d-md-flex image1 pt-5 mt-3">
              <Image src={Img1} className="" alt="" />
            </div>
            <div className="col-md-5 py-5">
              <h1 className="px-2 px-md-4">IELTS Exam Preparatory Class</h1>
              <p className="px-4 px-md-5 mx-1 mt-3 mt-md-0">
                If you are looking to study, develop a career, or settle in an
                English-speaking country, IELTS can pave the way for you in all
                these endeavors{' '}
              </p>
            </div>
            <div className="col-md image2 pb-5 mb-3">
              <Image src={Img2} className="" alt="" />
            </div>
          </div>
          <div className="radial1"></div>
        </div>
      </section>
    </>
  );
};

export default IeltsPreparatoryClass;
