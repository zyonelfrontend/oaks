'use client';
import Image from 'next/image';

const WhoWeAre = ({ title, subtitle, subtitle2, Img1, Img2 }) => {
  return (
    <>
      <section style={{ backgroundColor: '#fff' }} id="whoweare">
        <div className="container who_we_are pt-5">
          <div className="row align-items-center">
            <div className="col-md-6">
              <div className="yellow_back d-flex align-items-center justify-content-center">
                <Image
                  src={Img2}
                  className="relative d-flex align-items-center justify-content-center"
                  alt=""
                />
                <div className="image">
                  <Image
                    src={Img1}
                    width={520}
                    height={550}
                    className="ms-3 ms-md-4 mb-2"
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="ms-md-5 mt-5 mt-md-0">
                <h1 className="">{title}</h1>
                <p className="mt-4 mt-md-5 mb-3 mb-md-4">{subtitle}</p>
                <p className="">{subtitle2}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default WhoWeAre;
