import Image from 'next/image';
// import { tip, emma, artist, diversity, standout } from '../src/_assets/images'

export default function PostPreview({ post }) {
  return (
    <div className="card shadow rounded">
      <Image
        src={post.frontmatter.cover_image}
        width="500"
        height="200"
        className="card-img-top "
        alt="cover image"
      />
      <div className="card-body p-3">
        <h4 className="card-title pe-md-3 ">{post.frontmatter.title}</h4>
        <p className="card-text opas my-4 ft-1">{post.frontmatter.subtitle}</p>
      </div>
      {/* <div className='card-footer bg-white border-0 d-flex align-items-center'>
            <Image src={emma} width={40} height={40} alt='' className='rounded-5'/>
            <p className='mb-0 ms-3'>Lopez zee</p>
        </div> */}
    </div>
  );
}
