'use client';
import Image from 'next/image';

const TeachingMethods = ({ detailsInfo = [] }) => {
  return (
    <>
      <section>
        <div className="container" id="teachingmethod">
          <div className="methods">
            <h2 className="">Our Teaching Methods</h2>
            <div>
              <div className="row justify-content-center mx-3 mx-md-5 mt-5">
                {detailsInfo.map(({ border, icon, iconInfo }, index) => (
                  <div
                    key={`service-${index}`}
                    className={`col-md-6 col-lg-4 my-3`}
                  >
                    <div
                      className="p-2 p-md-3 d-flex align-items-center"
                      style={{ border, borderRadius: '5px' }}
                    >
                      <div className="icon">{icon}</div>
                      <p className="ms-3 ">{iconInfo}</p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default TeachingMethods;
