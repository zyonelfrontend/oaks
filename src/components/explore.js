'use client';
import Image from 'next/image';
import Link from 'next/link';
import GroupImage from '../../public/assets/images/groupie.png';
import ArrowRight from '../../public/assets/icon/yellow_arrow.svg';
import Slider from 'react-slick';
import Blog from '../../public/assets/icon/blog.svg';
import Video from '../../public/assets/icon/video.svg';
import Podcast from '../../public/assets/icon/microphone-2.svg';
import BlogArrow from '../../public/assets/icon/blog_icon.svg';
import VideoArrow from '../../public/assets/icon/video_icon.svg';
import PodcastArrow from '../../public/assets/icon/podcast.svg';

const Explore = () => {
  const explore = [
    {
      title: 'Our Blogs',
      icon: <Blog />,
      icon2: <BlogArrow />,
      description:
        'An extensive library of content backed by research and updated with the latest information on diverse study abroad topics.',
      hyperlink: 'Explore Blogs',
      border: '1px solid #D5A100',
      background: 'rgba(255, 251, 240, 1)',
      color_link: 'rgba(213, 161, 0, 1)',
      direction: '/blogs',
    },
    {
      title: 'Our Videos',
      icon: <Video />,
      icon2: <VideoArrow />,
      description:
        'From campus tours to visa updates to student life, watch exclusive videos to discover and learn more about student life at the top study destinations in the world',
      hyperlink: 'Watch Videos',
      border: '1px solid rgba(240, 89, 42, 0.48)',
      background: 'rgba(254, 248, 245, 1)',
      color_link: 'rgba(240, 89, 42, 1)',
      direction: '/about-us',
    },
    {
      title: 'Our Podcasts',
      icon: <Podcast />,
      icon2: <PodcastArrow />,
      description:
        'Now, you can listen to our study abroad experts, international students and the world best universities on the go as they share interesting facts and tips.',
      hyperlink: 'Listen Podcast',
      border: '1px solid rgba(43, 182, 156, 0.48)',
      background: 'rgba(247, 253, 252, 1)',
      color_link: 'rgba(43, 182, 156, 1)',
      direction: '/about-us',
    },
  ];
  return (
    <>
      <section className="explore">
        <div className="container">
          <h3 className="text-center">Explore more of our Services</h3>
          <div className="mt-md-5 pt-5">
            <div className="row justify-content-center justify-content-md-start justify-content-lg-center">
              {explore.map(
                (
                  {
                    title,
                    icon,
                    icon2,
                    description,
                    hyperlink,
                    border,
                    background,
                    color_link,
                    direction,
                  },
                  index,
                ) => (
                  <div
                    key={`explore-${index}`}
                    className="col-md-6 col-lg-4 my-3 explore_items"
                  >
                    <div
                      className="h-100 p-4 p-md-5 cards d-flex flex-column align-items-start"
                      style={{ border, background }}
                    >
                      {icon}
                      <h5 className="mt-4 mt-md-5 mb-3 mb-md-4">{title}</h5>
                      <p>{description}</p>
                      {/* <Link
                        style={{ color: color_link }}
                        className="link mt-4 mt-md-5"
                        href={direction}
                      >
                        {hyperlink}
                        <span className="ms-2">{icon2}</span>
                      </Link> */}
                    </div>
                  </div>
                ),
              )}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Explore;
