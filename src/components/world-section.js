'use client';
import Image from 'next/image';
import Link from 'next/link';

const WorldSection = () => {
  return (
    <>
      <section className="world-section">
        <div className="container">
          <div className="row align-items-center justify-content-center ">
            <div className="col-md-6">
              <div className="texts py-5">
                <h3 className="">
                  Trusted by over 100s of Students, Parents & Professionals
                  Worldwide. Since 2014.
                </h3>
                <div className="d-flex flex-column flex-md-row align-items-md-center my-5">
                  <div className="d-flex flex-column">
                    <span className="figure">100+</span>
                    <span className="result">Clients Reviews</span>
                  </div>
                  <div className="d-flex flex-column my-4 my-md-0 mx-md-4">
                    <span className="figure">55+</span>
                    <span className="result">Student Ratings</span>
                  </div>
                  <div className="d-flex flex-column">
                    <span className="figure">A+</span>
                    <span className="result">Grade Results</span>
                  </div>
                  <div className="d-flex flex-column my-4 my-md-0 mx-md-4">
                    <span className="figure">5+</span>
                    <span className="result">Countries</span>
                  </div>
                </div>
                <div className="contact ">
                  <button className="px-4 py-3">
                    <Link href="/about-us" className="">
                      Connect with us
                    </Link>
                  </button>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="map my-5 my-lg-0"></div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default WorldSection;
