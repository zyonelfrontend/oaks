import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import ArrowRight from './arrowRight';

const SuccessCard = ({
  title,
  image,
  description,
  description2,
  borderColor,
}) => {
  return (
    <div
      style={{ zIndex: '10' }}
      className="d-flex flex-column flex-md-row align-items-center movement"
    >
      <div className="success-card__image">
        <Image className="" src={image} alt={description} />
      </div>
      <div className="mx-3 mt-5">
        <h6 className="mb-2 success-card__title relative">{title}</h6>
        <div>
          <Image className="" src={borderColor} alt="border" />
        </div>
        <div className="my-4 success-card__description">{description}</div>
        <div className="success-card__description">{description2}</div>
        <Link
          href="/study-abroad"
          className="mt-4 success-card__link d-flex align-items-center"
        >
          Learn more <ArrowRight />
        </Link>
      </div>
    </div>
  );
};

export default SuccessCard;
