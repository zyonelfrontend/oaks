'use client';
import Link from 'next/link';
import ArrowRight from '../../public/assets/icon/white_arrow.svg';

const StartYourJourney = ({ imgDetails = [] }) => {
  return (
    <>
      <section>
        <div className="journey">
          <div className="titles text-center">
            <p className="main_title">OVERCOME YOUR BARRIERS</p>
            <h3 className="sub_title mt-md-4 pt-3 text-center">
              Start Your Journey of Excellence
            </h3>
            <h3 className="sub_title mt-md-4 pt-md-3 text-center">
              and Success Today
            </h3>
          </div>
          <div className="container">
            <div className="row justify-content-center">
              {imgDetails.map(({ bgImg, title, subtitle, links }, index) => (
                <div
                  key={`oasis-${index}`}
                  className="col-12 col-md-4 backgroundprops relative m-2"
                  style={{ backgroundImage: `url(${bgImg.src})` }}
                >
                  <div className="backdrop-blur p-3 d-flex flex-column justify-content-center">
                    <div>
                      <h4 className="text-white">{title}</h4>
                      <p className="text-white my-2 my-md-2 my-lg-3">
                        {subtitle}
                      </p>
                      <Link className="link" href={links}>
                        Learn More <ArrowRight />
                      </Link>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default StartYourJourney;
