'use client';
import Image from 'next/image';

const Team = ({ imgInfo, bgColor = '#258575' }) => {
  return (
    <>
      <section style={{ backgroundColor: bgColor }}>
        <div className="container team">
          <div className="px-4 px-md-0">
            <h5 className="">THE TEAM</h5>
            <div>
              <h1 className="my-2 my-lg-4 py-3 text-white">Meet Our Team</h1>
            </div>
            <div className="row">
              {imgInfo.map(({ bgImg, teamName, teamRole }, index) => (
                <div key={`oasis-${index}`} className="col-md-6 col-lg-3">
                  <div>
                    <div className="my-4 my-md-0">
                      <div className="team_img my-3">
                        <Image src={bgImg} alt="" />
                      </div>
                      <h4 className="my-2">{teamName}</h4>
                      <p className="">{teamRole}</p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Team;
