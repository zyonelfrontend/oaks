import Author from '../../../../public/assets/images/author_img.png';
import Image from 'next/image';
import fs from 'fs';
import path from 'path';
import matter from 'gray-matter';
import { remark } from 'remark';
import html from 'remark-html';
import MorePost from '@/components/MorePost';

export default function Blogdetail({ postData, otherPosts }) {
  return (
    <div className="blog-detail">
      <div className="green_bg d-flex flex-column align-items-center justify-content-center">
        <p className="mb-1">{postData.subtitle}</p>
        <h4 className="py-2 my-1 text-center    ">{postData.title}</h4>
        <p className="pb-4 mb-1">{postData.date}</p>
        <Image src={postData.cover_image} alt="..." width={1100} height={500} />
      </div>
      <div className="white_bg d-flex flex-column justify-content-center">
        <div
          dangerouslySetInnerHTML={{ __html: postData.contentHtml }}
          className=""
        />
        <hr />
        <div className="d-flex">
          <div>
            <Image src={Author} alt="..." />
          </div>
          <div className="ms-3 author">
            <h4 className="pb-1">About Author</h4>
            <h5 className="pt-1 name">
              Mojirade Osinoiki - Academic and Research Officer.
            </h5>
            <p className="mt-3">
              Mojirade Osinoiki is a staff at Oasis & Oaks Educational Training
              Consult as an Academic and Research Officer. She studied English
              Language at the University of Ibadan, Nigeria.  
            </p>
          </div>
        </div>
      </div>
      <MorePost otherPosts={otherPosts} />
    </div>
  );
}

export async function getStaticPaths() {
  const files = fs.readdirSync(path.join('posts'));

  const paths = files.map((filename) => ({
    params: {
      slug: filename.replace('.md', ''),
    },
  }));

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const { slug } = params;
  const fullPath = path.join('posts', slug + '.md');

  const fileContents = fs.readFileSync(fullPath, 'utf8');
  const matterResult = matter(fileContents);
  const processedContent = await remark()
    .use(html)
    .process(matterResult.content);
  const contentHtml = processedContent.toString();

  const postData = {
    slug,
    contentHtml,
    ...matterResult.data,
  };

  const files = fs.readdirSync(path.join('posts'));
  const otherPostSlugs = files
    .map((filename) => filename.replace('.md', ''))
    .filter((otherSlug) => otherSlug !== slug);
  const otherPosts = otherPostSlugs.map((otherSlug) => {
    const otherFullPath = path.join('posts', otherSlug + '.md');
    const otherFileContents = fs.readFileSync(otherFullPath, 'utf8');
    const otherMatterResult = matter(otherFileContents);

    return {
      slug: otherSlug,
      title: otherMatterResult.data.title,
      subtitle: otherMatterResult.data.subtitle,
      description: otherMatterResult.data.description,
      cover_image: otherMatterResult.data.cover_image,
      blogcover_image: otherMatterResult.data.blogcover_image,
      speaker_image: otherMatterResult.data.speaker_image,
      speaker_name: otherMatterResult.data.speaker_name,
    };
  });

  return {
    props: {
      postData,
      otherPosts,
    },
  };
}
