// import LatestPosts from '../../components/latest-posts';
import { getBlogsData } from '../../_helpers/utils/utils';
// import BlogBanner from '../../components/BlogBanner';
import IeltsPreparatoryClass from '@/components/ielts-preparatory-class';
import IeltsInfo from '@/components/ielts-info';
import SkillsIelts from '@/components/ielts-skills';
import FourSkillsIelts from '@/components/four-skills-ielts';
import Info from '@/components/info-ielts-visa';
import BlogBanner from '@/components/BlogBanner';
import LatestPosts from '@/components/latest-posts';
import Robot from '../../../public/assets/images/robot.png';
import Arrow from '../../../public/assets/icon/Up Left.svg';
import Image from 'next/image';
import Link from 'next/link';
import LatestPost from '@/components/LatestPost';
import { useRouter } from 'next/router';
import ServiceFormBanner from '@/components/serviceForm';
import Form from '@/components/form';
// import '../styles/blog.css';

const ServicesForm = () => {
  return (
    <>
      <ServiceFormBanner />
      <Form />
    </>
  );
};
export default ServicesForm;
