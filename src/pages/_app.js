// The default language for the conversation below is English unless a specific language is mentioned by the user.

// Import necessary libraries and styles
import { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '@/styles/index.scss';
import '@/styles/global.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

// Import components
import Layout from '@/components/layout';
import Image from 'next/image';
// import ScrollToTopOnPathChange from '@/components/scrollTop';

// Suppress console logs in production
if (process.env.NODE_ENV !== 'development') {
  console.log = () => {};
}

// Custom hook for Facebook Pixel tracking
function useFacebookPixel() {
  useEffect(() => {
    if (typeof window !== 'undefined') {
      !(function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
          n.callMethod
            ? n.callMethod.apply(n, arguments)
            : n.queue.push(arguments);
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s);
      })(
        window,
        document,
        'script',
        'https://connect.facebook.net/en_US/fbevents.js',
      );
      fbq('init', '1523094818573138');
      fbq('track', 'PageView');
    }
  }, []);
}

// Facebook Pixel tracking component
function FacebookPixelTracking() {
  useFacebookPixel();
  return (
    <noscript>
      <img
        height="1"
        width="1"
        alt="facebook"
        style={{ display: 'none' }}
        src="https://www.facebook.com/tr?id=1523094818573138&ev=PageView&noscript=1"
      />
    </noscript>
  );
}

// Main App component
export default function App({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
      <FacebookPixelTracking />
      {/* <ScrollToTopOnPathChange /> */}
    </Layout>
  );
}
