'use client';
import Image from 'next/image';
import Link from 'next/link';
import HomeBanner from '../../components/home-banner';
import SuccessCard from '../../components/successCard';
import SuccessStoryImage1 from '../../../public/assets/images/success_1.png';
import SuccessStoryImage2 from '../../../public/assets/images/success_2.png';
import SuccessStoryImage3 from '../../../public/assets/images/success_3.png';
import SuccessStoryImage4 from '../../../public/assets/images/success_4.png';
import SuccessStoryImage5 from '../../../public/assets/images/success_5.png';
import SuccessStoryImage6 from '../../../public/assets/images/success_6.png';
import BlurImage from '../../../public/assets/images/blur_bg.png';
import first from '../../../public/assets/images/first.png';
import second from '../../../public/assets/images/second.png';
import third from '../../../public/assets/images/third.png';
import fourth from '../../../public/assets/images/fourth.png';
import fifth from '../../../public/assets/images/fifth.png';
import sixth from '../../../public/assets/images/sixth.png';
import WhyOasis from '../../../public/assets/images/why-oasis.png';
import WorldMap from '../../../public/assets/images/world.png';
import UkFlag from '../../../public/assets/images/france.png';
import Canada from '../../../public/assets/images/toronto.png';
import Australia from '../../../public/assets/images/australia.png';
import Ireland from '../../../public/assets/images/ireland.png';
import NewZealand from '../../../public/assets/images/new-zealand.png';
import Usa from '../../../public/assets/images/usa.png';
import OvercomingYourBarrier from '../../components/overcome-your-barriers';
import WhyOasisSession from '../../components/why-oasis';
import WorldSection from '@/components/world-section';
import Where from '@/components/where';
import Review from '@/components/reviews';
import Explore from '@/components/explore';
import Layout from '@/components/layout';

const Home = () => {
  const successStories = [
    {
      title: 'Application Process',
      description: `We know that applying for a foreign university can be tedious and nerve-racking, and that is where we excel!`,
      description2: `We got you covered, as we can help you ace your international university application process without worry!`,
      image: SuccessStoryImage1,
      borderColor: first,
    },
    {
      title: 'Visa Application',
      description: `Worried about having your study visa application rejected?`,
      description2: `Our study destination experts can help you put to rest all your doubts about the process and help you in every step to ensure your student visa application process goes through smoothly until you get your visa!`,
      image: SuccessStoryImage2,
      borderColor: second,
    },
    {
      title: 'Scholarship Guidance',
      description: `Is availing Scholarships to study abroad very tricky?`,
      description2: `Not anymore! We can help you find the right one among the many for the course of your choice and provide proper scholarship guidance to ensure to receive them and fulfil your dream!`,
      image: SuccessStoryImage3,
      borderColor: third,
    },
    {
      title: 'Education Counselling',
      description: `Confused about which international course is the right one for you? Wondering which college will suit you best?`,
      description2: `Worry not; Our consultants are the best when it comes to guidance and counselling for studying abroad.`,
      image: SuccessStoryImage4,
      borderColor: fourth,
    },
    {
      title: 'Student Health Insurance',
      description: `To go abroad, you’ll need health insurance; as a student, you will need one even more! Wondering how to apply for them?`,
      description2: `We’ve got you covered! Just connect with us, and we will help you get the right international student health insurance for the duration of your course without any problems!`,
      image: SuccessStoryImage5,
      borderColor: fifth,
    },
    {
      title: 'Student Accommodation',
      description: `The accommodation makes all the difference when choosing to study abroad. Clueless about which will suit you?`,
      description2: `Consider it solved, as we have helped thousands of students get suitable student accommodation that doesn’t bite into their expenses, and we can help you too!`,
      image: SuccessStoryImage6,
      borderColor: sixth,
    },
  ];

  const studyLocations = [
    {
      image: Canada,
      country: 'Canada',
      title:
        'has a vast array of benefits for students and immigrants generally. It offers a safe and hospitable environment for students and travellers alike.',
    },
    {
      image: Australia,
      country: 'Australia',
      title:
        'home to world-renowned universities offering an array of courses and research opportunities. The quality of education is top-notch, of huge quality, and of course, recognized globally',
    },
    {
      image: UkFlag,
      country: 'United Kingdom',
      title:
        'Enrolling in any UK institution grants you access to top-tier education across various fields as well as state-of-the-art research infrastructures to further entrench learning.',
    },
    {
      image: Ireland,
      country: 'Ireland',
      title:
        'Diversity and open-mindedness are the fabrics of Ireland. This is the reason why thousands of international students have chosen to study in Ireland and make the country their permanent home in recent times.',
    },
    {
      image: Usa,
      country: 'United States of America',
      title:
        'is a good choice for travel, study and work because it presents various opportunities for both employment, education, and adventure. It offers a rich mosaic of traditions, languages,',
    },
    {
      image: NewZealand,
      country: 'New Zealand',
      title:
        'Diversity and open-mindedness are the fabrics of New Zealand. This is the reason why thousands of international students have chosen to study in New Zealand and make the country their permanent home in recent times.',
    },
  ];

  return (
    <>
      <HomeBanner />
      <OvercomingYourBarrier successStories={successStories} />
      <WhyOasisSession />
      <WorldSection />
      <Where studyLocations={studyLocations} />
      <Review />
      <Explore />
    </>
  );
};

export default Home;
