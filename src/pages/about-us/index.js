import SameBanner from '../../components/same-banner';
import BannerAboutUs from '../../../public/assets/images/banner-about-us.png';
import BannerAboutUs2 from '../../../public/assets/images/banner-about-us2.png';
import WhoWeAre from '../../components/who-we-are';
import WhoWeAreImg from '../../../public/assets/images/main-bg.png';
import WhoWeAreImg2 from '../../../public/assets/images/yellow-bg.png';
import WhyChooseOasis from '../../components/why-choose-oasis';
import Result from '../../../public/assets/icon/results.svg';
import Money from '../../../public/assets/icon/new_money.svg';
import Experience from '../../../public/assets/icon/new_excellence.svg';
import Ranking from '../../../public/assets/icon/experience.svg';
import Classes from '../../../public/assets/icon/classes.svg';
import Calender from '../../../public/assets/icon/new_schdule.svg';
import StartYourJourney from '../../components/start-your-journey2';
import Img10 from '../../../public/assets/images/new_academic.png';
import Img11 from '../../../public/assets/images/new_liason.png';
import Img12 from '../../../public/assets/images/Adesina.jpg';
import Img13 from '../../../public/assets/images/image-12.png';
import Img14 from '../../../public/assets/images/image-11.png';
import Img15 from '../../../public/assets/images/Edwin.jpeg';
import Img16 from '../../../public/assets/images/Walter.png';
import TeachingMethods from '../../components/our-teaching-method';
import TeachingMethodImg from '../../../public/assets/icon/method.svg';
import TeachingMethodImg2 from '../../../public/assets/icon/method_1.svg';
import TeachingMethodImg3 from '../../../public/assets/icon/method_2.svg';
import TeachingMethodImg4 from '../../../public/assets/icon/method_3.svg';
import TeachingMethodImg5 from '../../../public/assets/icon/method_4.svg';
import TeachingMethodImg6 from '../../../public/assets/icon/method_5.svg';
import Team from '../../components/team';
import Student from '@/components/student';
import Layout from '@/components/layout';

export default function AboutUs() {
  const chooseOasis = [
    {
      icon: <Result />,
      iconText: 'Proven Results',
    },
    {
      icon: <Money />,
      iconText: 'Affordable Cost',
    },
    {
      icon: <Ranking />,
      iconText: 'Wealth of experience',
    },
    {
      icon: <Experience />,
      iconText: 'Reputation for Excellence',
    },
    {
      icon: <Classes />,
      iconText: 'Flexible Classes',
    },
    {
      icon: <Calender />,
      iconText: 'Convenient Schedule',
    },
  ];
  const imgDetails = [
    {
      title: 'Study, Work & Live Abroad',
      subtitle:
        'An international degree gives you leverage in the global employment market. Start your study abroad journey with expert guide along the way.',
      bgImg: Img16,
      links: '/study-abroad',
    },
    {
      title: 'IELTS Exam Preparatory Class',
      subtitle:
        'An intensive class that prepares you for your IELTS exam with systematic lectures and step-by-step teaching method to pass IELTS in one attempt.',
      bgImg: Img14,
      links: '/ielts',
    },
    {
      title: 'Language Development',
      subtitle:
        'Learn and improve your speaking, reading, and writing ability in any language of your choice. Gain confidence in speaking and writing in 12 weeks',
      bgImg: Img13,
      links: '/ielts',
    },
  ];
  const teachingMethods = [
    {
      icon: <TeachingMethodImg />,
      iconInfo: 'Community Approach',
      border: '1px solid #3B68FF',
    },
    {
      icon: <TeachingMethodImg2 />,
      iconInfo: 'Student-centered Instruction',
      border: '1px solid #FBB183',
    },
    {
      icon: <TeachingMethodImg3 />,
      iconInfo: 'Experiential and Cooperative Learning',
      border: '1px solid #2BB69C',
    },
    {
      icon: <TeachingMethodImg4 />,
      iconInfo: 'Authentic Learning Materials',
      border: '1px solid #FFC325',
    },
    {
      icon: <TeachingMethodImg5 />,
      iconInfo: 'Integrated Grammar Instruction',
      border: '1px solid #FF86A8',
    },
    {
      icon: <TeachingMethodImg6 />,
      iconInfo: 'Online Dedicated Learning Support',
      border: '1px solid #9747FF',
    },
  ];
  const imgInfo = [
    {
      teamName: 'Ibukunoluwa Osinoiki',
      teamRole: 'Research & Academic Officer',
      bgImg: Img10,
    },
    {
      teamName: 'Omotoyosi Fawole',
      teamRole: 'Admissions Officer',
      bgImg: Img11,
    },
    {
      teamName: 'Adesina Abimbola',
      teamRole: 'Liason Officer',
      bgImg: Img12,
    },
    {
      teamName: 'Edwin Nnabude',
      teamRole: 'Marketing &  Communication',
      bgImg: Img15,
    },
  ];
  return (
    <>
      {/* <Layout> */}
      <SameBanner
        title="We are Educational & Resource Experts"
        subtitle="Oasis and Oaks is an educational training 
        and resource provider, that helps you achieve your goals. "
        BannerImg={BannerAboutUs}
        BannerImg2={BannerAboutUs2}
        reverse={true}
      />
      <WhoWeAre
        title="Who We Are"
        subtitle="Oasis and Oaks is an educational training and resource provider, established in 2014. 
        Our language services include in-depth trainings and preparations for IELTS, TOEFL, IGCSE examinations, French development classes and Adult Education."
        subtitle2="We also provide consultation on studying abroad, securing genuine admissions in foreign institutions and support programmes in organisational development."
        Img1={WhoWeAreImg}
        Img2={WhoWeAreImg2}
      />
      <WhyChooseOasis
        title="Why choose Oasis and Oaks?"
        chooseOasis={chooseOasis}
      />
      <StartYourJourney imgDetails={imgDetails} />
      <TeachingMethods detailsInfo={teachingMethods} />
      <Team imgInfo={imgInfo} />
      <Student />
      {/* </Layout> */}
    </>
  );
}
