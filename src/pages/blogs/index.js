import Arrow from '../../../public/assets/icon/Up Left.svg';
import Image from 'next/image';
import Link from 'next/link';
import LatestPost from '@/components/LatestPost';
import fs from 'fs';
import path from 'path';
import matter from 'gray-matter';

const Blog = ({ posts }) => {
  const firstPost = posts?.find(
    (post) =>
      post.frontmatter.title === '5 Packing Tips For Study Abroad Students',
  );

  return (
    <div className="blogs">
      {firstPost && (
        <>
          <div className="green_bg d-flex flex-column align-items-center justify-content-center">
            <p>{firstPost?.frontmatter.subtitle}</p>
            <h4 className="py-2 py-md-4 my-1 text-center">
              {firstPost?.frontmatter.title}
            </h4>
            <p className="pb-4 mb-1">{firstPost?.frontmatter.date}</p>
            <Image
              src={firstPost?.frontmatter.cover_image}
              alt="..."
              className=""
              width={1100}
              height={500}
            />
          </div>
          <div className="white_bg d-flex flex-column justify-content-center">
            <p>{firstPost?.frontmatter.description}</p>
            <Link
              href={'/blog-detail/' + firstPost.slug}
              className="link"
              passHref
              key={firstPost?.slug}
            >
              Read Post <Arrow className="arrow" />
            </Link>
          </div>
        </>
      )}

      <div className="category">
        <section className="container px-md-0">
          <div className="row py-3">
            <h4>Latest Posts</h4>
            <p className="py-3">Here are our latest posts so far.</p>
            {posts
              .filter(
                (post) =>
                  post?.frontmatter.title !==
                  '5 Packing Tips For Study Abroad Students',
              )
              .map((post, index) => (
                <div className="col-md-6" key={index}>
                  <LatestPost post={post} />
                </div>
              ))}
          </div>
        </section>
      </div>
    </div>
  );
};
export default Blog;

export async function getStaticProps() {
  const files = fs.readdirSync(path.join('posts'));

  const posts = files.map((filename) => {
    const slug = filename.replace('.md', '');
    const markdownWithMeta = fs.readFileSync(
      path.join('posts', filename),
      'utf-8',
    );
    const { data: frontmatter } = matter(markdownWithMeta);
    return {
      slug,
      frontmatter,
    };
  });
  return {
    props: {
      posts,
    },
  };
}
