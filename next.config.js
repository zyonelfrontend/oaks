/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/i,
      // issuer: /\.[jt]sx?$/,
      use: ['@svgr/webpack'],
    });
    // config.resolve.fallback = {
    //   fs: false,
    //   path: false,
    // };
    config.resolve.alias.canvas = false;

    return config;
  },
  images: {
    // formats: ["image/avif", "image/webp"],
    domains: ['s3-eu-central-1.amazonaws.com', 'www.facebook.com'],
  },
};

module.exports = nextConfig;
